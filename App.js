import React, { useState } from 'react';
import { View, StyleSheet, Button, Text, Image, Alert } from 'react-native';


const App = () => {
  const kelvin = 273.15
  const [Clima, setClima] = useState({})

  function consultar() {
    fetch('http://api.openweathermap.org/data/2.5/weather?q=Allentown,us&appid=16c58fadc9313c18ae1ae8035254a1bb')
      .then(response => response.json())
      .then(data => setClima(data)).catch(()=> Alert.alert('error', 'error', [{text:'OK'}]));
  }
  return (
    <>
      <View style={styles.app}>
        <View style={styles.content}>
          <View style={{marginBottom:50}}>
            {Object.keys(Clima).length === 0 ? null :
              (
                <>
                  <Text style={styles.title}>{Clima.name}, {Clima.sys.country}</Text>
                  <Text style={[styles.text, styles.actual]}>{ parseInt(Clima.main.temp - kelvin)} 

                  <Text style={styles.temp}>&#x2103;</Text>
                  <Image style={{width:66, height:58}}
                  source={{uri: `http://openweathermap.org/img/w/${Clima.weather[0].icon}.png`}}/>
                  </Text>
                  
                  <View style={styles.temps}>
                    <Text style={styles.text}>Min {' '}
                      <Text style={styles.temp}>
                        {parseInt(Clima.main.temp_min - kelvin)} &#x2103;
                      </Text>
                    </Text>

                    <Text style={styles.text}>Max {' '}
                      <Text style={styles.temp}>
                        {parseInt(Clima.main.temp_max - kelvin)} &#x2103;
                      </Text>
                    </Text>
                  </View>
                </>
              )
            }

          </View>
          <Button color='black' title='Buscar' onPress={() => consultar()} />
        </View>
      </View>
    </>

  );
};
const styles = StyleSheet.create({
  app: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgb(71,149,212)'
  },
  content: {
    marginHorizontal: '2.5%'
  },
  text: {
    color: '#FFF',
    fontSize: 20,
    textAlign: 'center',
    marginRight: 20
  },
  actual: {
    fontSize: 80,
    marginRight: 0,
    fontWeight: 'bold'
  },
  temp: {
    fontSize: 24,
    fontWeight: 'bold'
  },
  temps: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 45,
    textAlign:'center',
    color:'#FFF'
  }
})
export default App;
